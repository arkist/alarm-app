export const ACTIONS = {
    SOUND: 'SOUND',
    VIBE: 'VIBE',
    NOOP: 'NOOP'
};
export const ACTIONS_KOR = {
    SOUND: '소리',
    VIBE: '진동',
    NOOP: 'NOOP'
};
export const WATCH_MODES = {
    NORMAL: 'NORMAL',
    VIBE: 'VIBE',
    NIGHT: 'NIGHT'
};
export const WATCH_MODES_KOR = {
    NORMAL: '일반',
    VIBE: '진동',
    NIGHT: '야간'
};
export const ALARM_MODES = {
    NORMAL: 'NORMAL',
    URGENCY: 'URGENCY'
};
export const ALARM_MODES_KOR = {
    NORMAL: '일반',
    URGENCY: '긴급'
};
