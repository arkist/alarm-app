import {ACTIONS, ALARM_MODES, WATCH_MODES} from './constants';

export default class Alarm {
    constructor(options) {
        const {mode, text, hours, minutes} = options;
        this.id = new Date().getTime()+'';
        this.isOff = false;
        this.mode = mode;
        this.text = text;
        this.hours = hours;
        this.minutes = minutes;
    }

    toggle() {
        this.isOff = !this.isOff;
    }

    checkAlarm(alarmType, time) {
        if (this.isOff) {
            return false;
        }

        const hours = time.getHours();
        const minutes = time.getMinutes();
        if (this.hours !== hours || this.minutes !== minutes) {
            return false;
        }

        if (alarmType === ACTIONS.NOOP) {
            return false;
        }

        return true;
    }

    getAlarmType(watchMode) {
        switch (watchMode) {
            case WATCH_MODES.NIGHT:
                if (this.mode === ALARM_MODES.URGENCY) {
                    return ACTIONS.SOUND;
                }
                return ACTIONS.NOOP;
            case WATCH_MODES.VIBE:
                return ACTIONS.VIBE;
            default:
                return ACTIONS.SOUND;
        }
    }

    isDuplicatedAlarm(alarm) {
        if (
            this.hours === alarm.hours &&
            this.minutes === alarm.minutes &&
            this.text === alarm.text
        ) {
            return true;
        }

        return false;
    }
}
