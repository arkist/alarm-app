import {ALARM_MODES_KOR} from '../constants';

export default class AlarmListView {
    constructor($rootEl, controller) {
        this.controller = controller;
        this.$alarmList = $rootEl.find('.alarm-list');

        this.toggleAlarmHandler = this.toggleAlarmHandler.bind(this);
        this.removeAlarmHandler = this.removeAlarmHandler.bind(this);
        $rootEl.find('.alarm-list')
            .on('click', '.toggle-alarm-btn', this.toggleAlarmHandler)
            .on('click', '.remove-alarm-btn', this.removeAlarmHandler);
    }

    toggleAlarmHandler(evt) {
        const id = getItemId($(evt.currentTarget));
        this.controller.toggleAlarmById(id);
    }

    removeAlarmHandler(evt) {
        const id = getItemId($(evt.currentTarget));
        this.controller.removeAlarmById(id);
    }

    setAlarmList(items) {
        this.$alarmList.html(items.map(generateItemDOM));
    }
}

function generateItemDOM(alarm) {
    const className = [
        'item',
        alarm.isOff ? 'off' : ''
    ].join(' ');
    const time = `${alarm.hours}:${alarm.minutes}`;
    const mode = ALARM_MODES_KOR[alarm.mode];
    const msg = `<span class="text">${alarm.text}</span>`;
    const toggleText = alarm.isOff ? '켜기' : '끄기';
    const offToggleBtn = generateButtonDOM('toggle-alarm-btn', toggleText);
    const removeBtn = generateButtonDOM('remove-alarm-btn', '삭제');
    const innerHTML = `${time} ${mode} - ${msg} ${offToggleBtn} ${removeBtn}`;

    return `<li data-id="${alarm.id}" class="${className}">${innerHTML}</li>`;
}

function generateButtonDOM(className, text) {
    return `<button class="${className}" type="button">${text}</button>`;
}

export function getItemId($el) {
    return $el.parent('.item').data('id');
}