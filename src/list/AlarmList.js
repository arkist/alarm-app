import AlarmListView from "./AlarmListView";

export default class AlarmList {
    constructor($rootEl) {
        this.alarms = [];
        this.view = new AlarmListView($rootEl, this);
    }

    toggleAlarmById(id) {
        this.alarms.find(alarm => alarm.id === id + '').toggle();

        this.updateAlarmList();
    }

    removeAlarmById(id) {
        this.alarms = this.alarms.filter(alarm => alarm.id !== id + '');

        this.updateAlarmList();
    }

    addAlarm(alarm) {
        this.alarms.push(alarm);

        this.updateAlarmList();
    }

    hasDuplicatedAlarm(alarm) {
        for (let i = 0, len = this.alarms.length; i < len; i++) {
            if (this.alarms[i].isDuplicatedAlarm(alarm)) {
                return true;
            }
        }

        return false;
    }

    checkAlarms(time, watchMode, callback) {
        this.alarms.forEach(alarm => {
            const alarmType = alarm.getAlarmType(watchMode);
            const confirmedAlarm = alarm.checkAlarm(alarmType, time);

            if (confirmedAlarm) {
                callback(alarmType, alarm);
            }
        });
    }

    updateAlarmList() {
        const items = this.alarms.sort((a, b) => {
            const minutes = alarm => (alarm.hours * 60 + alarm.minutes);
            return minutes(a) - minutes(b);
        });

        this.view.setAlarmList(items);
    }
}

