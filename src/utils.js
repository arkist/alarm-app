export function initSelectOptions($el, values, labels) {
    const options = Object.keys(values).map(key => {
        return `<option value="${values[key]}">${labels[key]}</option>`;
    });

    $el.append(options);
}

export function getTimes(val) {
    const times = val.split(':');
    const hours = parseInt(times[0], 10);
    const minutes = times[1] ? parseInt(times[1], 10) : undefined;

    return {
        hours,
        minutes
    };
}

export function isValidTime(hours, minutes) {
    if (typeof hours !== 'number' || typeof minutes !== 'number') {
        return false;
    }

    if (hours < 0 || hours > 23) {
        return false;
    }

    if (minutes < 0 || minutes > 59) {
        return false;
    }

    return true;
}
