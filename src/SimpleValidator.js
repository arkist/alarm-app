import {isValidTime} from './utils';

export default class SimpleValidator {
    validate(alarm) {
        const {text, hours, minutes} = alarm;
        if (!text || !isValidTime(hours, minutes)) {
            alert('시간과 내용을 확인해주세요.');
            return false;
        }

        return true;
    }
}
