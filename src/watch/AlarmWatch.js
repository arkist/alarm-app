import {WATCH_MODES, WATCH_MODES_KOR} from '../constants';
import {getTimes, isValidTime} from '../utils';
import AlarmWatchView from "./AlarmWatchView";

export default class AlarmWatch {
    constructor($rootEl) {
        this.currentWatchMode = WATCH_MODES.NORMAL;
        this.currentTime = new Date();
        this.view = new AlarmWatchView($rootEl, this);

        this.setWatchMode(this.currentWatchMode);
    }

    setCurrentTime(val) {
        this.currentTime = val;
        this.setCurrentTimeView(val);
    }

    setCurrentTimeView(time) {
        const hours = time.getHours();
        const minutes = time.getMinutes();
        const seconds = time.getSeconds();
        this.view.setCurrentTime(`${hours}:${minutes}:${seconds}`);
    }

    setWatchMode(watchMode) {
        this.view.setWatchMode(WATCH_MODES_KOR[watchMode]);
    }

    validateTime(time) {
        const {hours, minutes} = getTimes(time);

        return isValidTime(hours, minutes);
    }

    getHoursAndMinutesFromTime(time) {
        return getTimes(time);
    }
}