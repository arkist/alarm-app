import {WATCH_MODES, WATCH_MODES_KOR} from '../constants';
import {initSelectOptions} from '../utils';

export default class AlarmWatchView {
    constructor($rootEl, controller) {
        this.controller = controller;
        this.$currentWatchMode = $rootEl.find('.current-watch-mode');
        this.$currentTimeText = $rootEl.find('.current-time-text');
        this.$watchModeSelect = $rootEl.find('select[name="watch-mode"]');
        this.$form = $rootEl.find('.watch-form');

        this.changeWatchModeHandler = this.changeWatchModeHandler.bind(this);
        this.setWatchHandler = this.setWatchHandler.bind(this);

        this.$watchModeSelect.on('change', this.changeWatchModeHandler);
        this.$form.on('submit', this.setWatchHandler);

        initSelectOptions(this.$watchModeSelect, WATCH_MODES, WATCH_MODES_KOR);
    }

    changeWatchModeHandler(evt) {
        const watchMode = this.$watchModeSelect.val();
        this.currentWatchMode = watchMode;
        this.controller.setWatchMode(watchMode);
    }

    setWatchHandler(evt) {
        evt.preventDefault();

        const $form = $(evt.currentTarget);
        const time = $form.find('[name="time"]').val();

        if (!this.controller.validateTime(time)) {
            alert('시간을 확인해 주세요.');
            return;
        }

        const {hours, minutes} = this.controller.getHoursAndMinutesFromTime(time);

        this.controller.setCurrentTime(new Date(0, 0, 0, hours, minutes));
    }

    setWatchMode(html) {
        this.$currentWatchMode.html(html);
    }

    setCurrentTime(html) {
        this.$currentTimeText.html(html);
    }
}