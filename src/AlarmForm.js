import {getTimes, initSelectOptions} from './utils';
import {ALARM_MODES, ALARM_MODES_KOR} from './constants';
import Alarm from './Alarm';
import SimpleValidator from './SimpleValidator';

export default class AlarmForm {
    constructor($rootEl, alarmList) {
        this.alarmList = alarmList;
        this.validator = new SimpleValidator();
        this.$form = $rootEl.find('.alarm-form');
        this.$alarmModeSelect = this.$form.find('select[name="alarm-mode"]');

        initSelectOptions(this.$alarmModeSelect, ALARM_MODES, ALARM_MODES_KOR);

        this.addAlarmHandler = this.addAlarmHandler.bind(this);
        this.$form.on('submit', this.addAlarmHandler);
    }

    addAlarmHandler(evt) {
        evt.preventDefault();

        const $form = $(evt.currentTarget);
        const mode = $form.find('[name="alarm-mode"]').val();
        const text = $form.find('[name="text"]').val();
        const timeVal = $form.find('[name="time"]').val();

        const {hours, minutes} = getTimes(timeVal);

        const alarm = new Alarm({
            mode,
            text,
            hours,
            minutes
        });

        if (!this.validator.validate(alarm)) {
            return;
        }

        if (this.alarmList.hasDuplicatedAlarm(alarm)) {
            return;
        }

        this.alarmList.addAlarm(alarm);
    }
}