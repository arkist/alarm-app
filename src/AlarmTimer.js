import AlarmWatch from './watch/AlarmWatch';
import AlarmList from './list/AlarmList';
import AlarmForm from './AlarmForm';
import AlarmLog from './AlarmLog';

export default class AlarmTimer {
    constructor($rootEl) {
        this.alarmWatch = new AlarmWatch($rootEl);
        this.alarmList = new AlarmList($rootEl);
        this.alarmForm = new AlarmForm($rootEl, this.alarmList);
        this.alarmLog = new AlarmLog($rootEl);
    }

    startTimer() {
        this.tick(this.alarmWatch.currentTime);

        setInterval(() => {
            const time = new Date(this.alarmWatch.currentTime.getTime() + 1000);
            this.tick(time);
        }, 1000);
    }

    tick(time) {
        this.alarmWatch.setCurrentTime(time);

        if (time.getSeconds() === 0) {
            const watchMode = this.alarmWatch.currentWatchMode;

            this.alarmList.checkAlarms(time, watchMode, (alarmType, alarm) => {
                 this.alarmLog.logAlarm(alarmType, alarm);
            });
        }
    }
}
