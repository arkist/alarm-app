import {ACTIONS_KOR, ALARM_MODES_KOR} from './constants';

export default class AlarmLog {
    constructor($rootEl) {
        this.$alarmsLog = $rootEl.find('.alarms-log');
    }

    logAlarm(alarmType, alarm) {
        const msg = getAlarmMessage(alarmType, alarm);

        this.$alarmsLog.append(`<p>${msg} - ${new Date()}</p>`);
    }
}

function getAlarmMessage(alarmType, alarm) {
    const type = ACTIONS_KOR[alarmType];
    const mode = ALARM_MODES_KOR[alarm.mode];

    return `[${type}][${mode}] ${alarm.text}`;
}