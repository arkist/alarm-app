/**
 * Alarm - Kakao pretest
 *
 * 1. 어플리케이션 구조를 결정하는 라이브러리를 쓰지 말라는 기준이 모호해서
 * jquery만 사용해서 간단히 구현하였음. 템플릿엔진도 안쓰고 문자열로 조립.
 * 2. '로컬에서 실행가능한 형태'에 대한 내용이 있어서 별도의 빌드스텝을 넣지 못해
 * 구현상 제약이 있었음.
 *     - 모듈화 못함. 그렇다고 AMD는 쓰기 싫었음.
 *     - 응답형 UI를 구현하지 못해 전역변수를 사용하고 수동 UI 갱신코드가 들어감.
 *     - HTML5 지원이라는 기준이 모호해 유용한 최신 문법을 사용하지 못해 코드가 번잡한 감이 있음.
 *     - 올바른 테스트 환경을 구성할 수 없어서 테스트코드는 포기. 주석으로 타입힌트와 설명만.
 * 3. 알람은 매 0초마다 (즉, 1분에 한번) 체크하도록 구현해 놓음. 입력을 분단위로 받기 때문에
 * 옳은 구현이라 생각하지만 테스트하긴 귀찮을 것.
 * 4. 테스트하기 쉽도록 정보은닉을 위한 클로저를 생성하지 않고 전역에 모든것을 던져놓았음.
 * 실제 작업시에는 이런식으로 구현하진 않음.
 */
'use strict';

/**
 * @typedef {Object} Alarm
 * @property {string} id - unique number (use timestamp currently)
 * @property {boolean} isOff
 * @property {string} mode - one of WATCH_MODES
 * @property {string} text
 * @property {number} hours - 0~11
 * @property {number} minutes - 0~59
 */

var ACTIONS = {
    SOUND: 'SOUND',
    VIBE: 'VIBE',
    NOOP: 'NOOP'
};
var ACTIONS_KOR = {
    SOUND: '소리',
    VIBE: '진동',
    NOOP: 'NOOP'
};
var WATCH_MODES = {
    NORMAL: 'NORMAL',
    VIBE: 'VIBE',
    NIGHT: 'NIGHT'
};
var WATCH_MODES_KOR = {
    NORMAL: '일반',
    VIBE: '진동',
    NIGHT: '야간'
};
var ALARM_MODES = {
    NORMAL: 'NORMAL',
    URGENCY: 'URGENCY'
};
var ALARM_MODES_KOR = {
    NORMAL: '일반',
    URGENCY: '긴급'
};

var shared = {
    alarms: [/** @type {Alarm} */],
    currentTime: new Date(),
    currentWatchMode: WATCH_MODES.NORMAL
};

var $currentWatchMode = $('.current-watch-mode');
var $currentTimeText = $('.current-time-text');
var $alarmList = $('.alarm-list');
var $alarmsLog = $('.alarms-log');
var $watchModeSelect = $('select[name="watch-mode"]');
var $alarmModeSelect = $('select[name="alarm-mode"]');

/**
 * @type {isValidValues}
 */
var isValidValues = isValidValuesFunc();

initialize();

/**
 * UI 요소 초기화와 이벤트 바인딩
 */
function initialize() {
    initAllSelectOptions();
    startTimer();
    setWatchMode(shared.currentWatchMode);

    $watchModeSelect.on('change', changeWatchMode);
    $('.watch-form').on('submit', setWatchHandler);
    $('.alarm-form').on('submit', addAlarmHandler);
    $('.alarm-list')
        .on('click', '.toggle-alarm-btn', toggleAlarmHandler)
        .on('click', '.remove-alarm-btn', removeAlarmHandler);
}

function changeWatchMode(evt) {
    var watchMode = $watchModeSelect.val();
    shared.currentWatchMode = watchMode;
    setWatchMode(watchMode);
}

function setWatchMode(watchMode) {
    $currentWatchMode.html(WATCH_MODES_KOR[watchMode]);
}

function toggleAlarmHandler(evt) {
    var id = getItemId($(evt.currentTarget));
    toggleAlarm(id);
}

function removeAlarmHandler(evt) {
    var id = getItemId($(evt.currentTarget));
    removeAlarm(id);
}

/**
 * @param {JQuery} $el
 * @return {string|number}
 */
function getItemId($el) {
    return $el.parent('.item').data('id');
}

function setWatchHandler(evt) {
    evt.preventDefault();

    var $form = $(evt.currentTarget);
    var timeVal = $form.find('[name="time"]').val();

    var times = getTimes(timeVal);
    var hours = times.hours;
    var minutes = times.minutes;

    if (!isValidTime(hours, minutes)) {
        alert('시간을 확인해 주세요.');
        return;
    }

    shared.currentTime = new Date(0, 0, 0, hours, minutes);
}

function addAlarmHandler(evt) {
    evt.preventDefault();

    var $form = $(evt.currentTarget);
    var mode = $form.find('[name="alarm-mode"]').val();
    var text = $form.find('[name="text"]').val();
    var timeVal = $form.find('[name="time"]').val();

    if (!isValidValues(text, timeVal)) {
        return;
    }

    var times = getTimes(timeVal);
    var hours = times.hours;
    var minutes = times.minutes;

    /**
     * @type {Alarm}
     */
    var alarm = {
        id: new Date().getTime() + '',
        isOff: false,
        mode: mode,
        text: text,
        hours: hours,
        minutes: minutes
    };

    addAlarm(alarm);
}

/**
 * 알람 목록에 아이템을 더하고 목록 UI 업데이트.
 * @param {Alarm} alarm
 */
function addAlarm(alarm) {
    shared.alarms.push(alarm);

    updateAlarmList();
}

/**
 * 특정 아이템을 알람 목록에서 제거하고 목록 UI 업데이트.
 * @param {number|string} id
 */
function removeAlarm(id) {
    var alarms = shared.alarms;
    for (var i = 0, len = alarms.length; i < len; i++) {
        if (alarms[i].id === id+'') {
            alarms.splice(i, 1);
            break;
        }
    }

    updateAlarmList();
}

/**
 * 특정 아이템을 토글하고 목록 UI 업데이트.
 * @param {number|string} id
 */
function toggleAlarm(id) {
    var alarms = shared.alarms;
    var alarm;
    for (var i = 0, len = alarms.length; i < len; i++) {
        alarm = alarms[i];
        if (alarm.id === id+'') {
            alarm.isOff = !alarm.isOff;
            break;
        }
    }

    updateAlarmList();
}

/**
 * 알람 목록 UI 업데이트.
 * 응답형 라이브러리를 쓰지 못했으므로 별도로 함수로 빼서 수동으로 UI를 갱신해준다.
 */
function updateAlarmList() {
    var items = shared.alarms.sort(function (a, b) {
        return (a.hours * 60 + a.minutes) - (b.hours * 60 + b.minutes);
    }).map(generateItemDOM);

    $alarmList.html(items);
}

/**
 * @param {Alarm} item
 * @returns {string} DOM
 */
function generateItemDOM(item) {
    var className = [
        'item',
        item.isOff ? 'off' : ''
    ].join(' ');
    var time = item.hours + ':' + item.minutes;
    var mode = ALARM_MODES_KOR[item.mode];
    var msg = '<span class="text">' + item.text + '</span>';
    var toggleText = item.isOff ? '켜기' : '끄기';
    var offToggleBtn = '<button class="toggle-alarm-btn" type="button">' + toggleText + '</button>';
    var removeBtn = '<button class="remove-alarm-btn" type="button">삭제</button>';
    var html = time + ' ' + mode + ' - ' + msg + ' ' + offToggleBtn + ' ' + removeBtn;

    return '<li data-id="' + item.id + '" class="' + className + '">' + html + '</li>';
}

/**
 * 간단한 유효성 검사 함수
 * 중복등록 방지를 위한 최근 아이템과의 비교에 클로져를 사용해 봄.
 * @return {Function}
 */
function isValidValuesFunc() {
    var recentItem = {};
    /**
     * 간단한 유효성 검사
     * @callback isValidValues
     * @param {string} text
     * @param {string} timeVal
     * @return {boolean} isValid
     */
    return function (text, timeVal) {
        var times = getTimes(timeVal);
        var hours = times.hours;
        var minutes = times.minutes;

        if (!text || !timeVal || !isValidTime(hours, minutes)) {
            alert('시간과 내용을 확인해주세요.');
            return false;
        }

        // 중복등록 방지
        if (
            recentItem.hours === hours &&
            recentItem.minutes === minutes &&
            recentItem.text === text
        ) {
            return false;
        }

        recentItem = {
            hours: hours,
            minutes: minutes,
            text: text
        };

        return true;
    };
}

/**
 * 입력된 시간이 유효한 값인지 확인한다
 * @param {number} hours
 * @param {number} minutes
 * @return {boolean}
 */
function isValidTime(hours, minutes) {
    if (typeof hours !== 'number' || typeof minutes !== 'number') {
        return false;
    }

    if (hours < 0 || hours > 23) {
        return false;
    }

    if (minutes < 0 || minutes > 59) {
        return false;
    }

    return true;
}

/**
 * @example
 * // returns {hours: 1, 12}
 * getTimes('01:12');
 * @param val
 * @return {{hours: number, minutes: number}}
 */
function getTimes(val) {
    var times = val.split(':');
    var hours = parseInt(times[0], 10);
    var minutes = times[1] ? parseInt(times[1], 10) : undefined;

    return {
        hours: hours,
        minutes: minutes
    };
}

/**
 * select 엘리먼트들에 option들 할당.
 * value, label등 값이 파편화 되는것을 막기위해 js에서 상수를 쓰고 만들어준다.
 * React.js 같은걸 썼으면 이렇게 구현하지 않았을듯.
 */
function initAllSelectOptions() {
    var watchModeItems = {};
    watchModeItems[WATCH_MODES.NORMAL] = WATCH_MODES_KOR.NORMAL;
    watchModeItems[WATCH_MODES.NIGHT] = WATCH_MODES_KOR.NIGHT;
    watchModeItems[WATCH_MODES.VIBE] = WATCH_MODES_KOR.VIBE;

    initOptions($watchModeSelect, watchModeItems);

    var alarmModeItems = {};
    alarmModeItems[ALARM_MODES.NORMAL] = ALARM_MODES_KOR.NORMAL;
    alarmModeItems[ALARM_MODES.URGENCY] = ALARM_MODES_KOR.URGENCY;

    initOptions($alarmModeSelect, alarmModeItems);
}

function initOptions($el, items) {
    var options = Object.keys(items).map(function (key) {
        var label = items[key];
        return '<option value="' + key + '">' + label + '</option>';
    });
    $el.append(options);
}

/**
 * 기준시간은 수동 지정이 가능하기 때문에 Date 객체를 바로 쓸 수 없다.
 * 매초마다 수동으로 타이머를 제어해줌.
 */
function startTimer() {
    timer(shared.currentTime);
    setInterval(function () {
        var time = new Date(shared.currentTime.getTime() + 1000);
        shared.currentTime = time;
        timer(time);
    }, 1000);
}

/**
 * 시간 UI를 업데이트 해주고, 매 0초마다 알람 리스트를 체크한다.
 * @param time
 */
function timer(time) {
    var hours = time.getHours();
    var minutes = time.getMinutes();
    var seconds = time.getSeconds();

    $currentTimeText.html(hours + ':' + minutes + ':' + seconds);

    if (seconds === 0) {
        shared.alarms.forEach(function (item) {
            checkAlarm(item, hours, minutes);
        });
    }
}

/**
 * 알람 조건들을 체크하고, 충족하면 알람 로그에 아이템을 추가한다
 * @param {Alarm} alarm
 * @param {number} hours
 * @param {number} minutes
 */
function checkAlarm(alarm, hours, minutes) {
    if (!isCorrectAlarm(alarm, hours, minutes)) {
        return;
    }

    var alarmType = getAlarmType(alarm.mode, shared.currentWatchMode);
    if (alarmType === ACTIONS.NOOP) {
        return;
    }

    var type = ACTIONS_KOR[alarmType];
    var mode = ALARM_MODES_KOR[alarm.mode];
    var msg = '[' + type + '][' + mode + '] ' + alarm.text + ' - ' + new Date();

    $alarmsLog.append('<p>' + msg + '</p>')
}

/**
 * 알람이 꺼짐상태인지 먼저 체크하고, 시/분을 체크해서 현재 울릴 녀석이 맞는지 체크
 * @param {Alarm} alarm
 * @param {number} hours
 * @param {number} minutes
 * @return {boolean}
 */
function isCorrectAlarm(alarm, hours, minutes) {
    if (alarm.isOff) {
        return false;
    }

    return alarm.hours === hours && alarm.minutes === minutes;
}

/**
 * @param {string} alarmMode - one of ALARM_MODES
 * @param {string} watchMode - one of WATCH_MODES
 * @return {string} alarmType - one of ACTIONS
 */
function getAlarmType(alarmMode, watchMode) {
    switch (watchMode) {
        case WATCH_MODES.NIGHT:
            if (alarmMode === ALARM_MODES.URGENCY) {
                return ACTIONS.SOUND;
            }
            return ACTIONS.NOOP;
        case WATCH_MODES.VIBE:
            return ACTIONS.VIBE;
        default:
            return ACTIONS.SOUND;
    }
}

