const Express = require('express');
const fs = require('fs');

const app = new Express();

app.use('/', Express.static(__dirname));

app.listen(4000, (err) => {
    if (err) {
        console.error(err);
        return;
    }
    console.info('server listening on :4000');
});
